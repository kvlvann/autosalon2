import Classes.*;

public class Main {
    public static void main(String[] args) {

                Autosalon autosalon = new Autosalon();

                OsobniAuto osobniAuto1 = new OsobniAuto("Toyota Corolla", 190, 6.8, "benzin", 5);
                autosalon.pridatVozidlo(osobniAuto1);

                NakladniVoz nakladniVoz1 = new NakladniVoz("Volvo FH16", 120, 25.0, "nafta", 27000);
                autosalon.pridatVozidlo(nakladniVoz1);

                Motocykl motocykl1 = new Motocykl("Harley-Davidson Iron 883", 160, 4.5, "benzin", "zelený");
                autosalon.pridatVozidlo(motocykl1);

                Vozidlo hledaneVozidlo = autosalon.najitVozidlo("Toyota Corolla");
                if (hledaneVozidlo != null) {
                    System.out.println("Hledaný vůz: " + hledaneVozidlo.getNazev()+ ". Max.rychlost: " +hledaneVozidlo.getMaxRychlost()
                            +"; spot5eba paliva: "+ hledaneVozidlo.getSpotrebaPaliva()+"; typ pohonu: "+ hledaneVozidlo.getTypPohonu()+"." );
                } else {
                    System.out.println("Hledaný vůz nebyl nalezen.");
                }
            }
        }