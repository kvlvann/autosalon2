package Classes;

public class Motocykl extends Vozidlo{

    private String barva;

    public Motocykl(String nazev, int maxRychlost, double spotrebaPaliva, String typPohonu, String barva) {
        super(nazev, maxRychlost, spotrebaPaliva, typPohonu);
        this.barva = barva;
    }

    public String getBarva() {
        return barva;
    }

    public void setBarva(String barva) {
        this.barva = barva;
    }
}
