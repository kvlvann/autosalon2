package Classes;

import java.util.ArrayList;

public class Autosalon {
    private ArrayList<Vozidlo> listAut;

    public Autosalon() {
        this.listAut = new ArrayList<>();
    }

    public void pridatVozidlo(Vozidlo vozidlo) {
        listAut.add(vozidlo);
    }

    public void odebratVozidlo(Vozidlo vozidlo) {
        listAut.remove(vozidlo);
    }

    public Vozidlo najitVozidlo(String nazev) {
        for (Vozidlo vozidlo : listAut) {
            if (vozidlo.getNazev().equals(nazev)) {
                return vozidlo;
            }
        }
        return null;
    }
}