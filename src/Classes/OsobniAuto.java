package Classes;

public class OsobniAuto extends Vozidlo {
    private int pocetPasazeru;

    public OsobniAuto(String nazev, int maxRychlost, double spotrebaPaliva, String typPohonu, int pocetPasazeru) {
        super(nazev, maxRychlost, spotrebaPaliva, typPohonu);
        this.pocetPasazeru = pocetPasazeru;
    }

    public int getPocetPasazeru() {
        return pocetPasazeru;
    }

    public void setPocetPasazeru(int pocetPasazeru) {
        this.pocetPasazeru = pocetPasazeru;
    }
}

