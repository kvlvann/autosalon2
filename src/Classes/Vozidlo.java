package Classes;

public class Vozidlo {
    private String nazev;
    private int maxRychlost;
    private double spotrebaPaliva;
    private String typPohonu;

        public Vozidlo (String nazev, int maxRychlost, double spotrebaPaliva, String typPohonu) {
        this.nazev = nazev;
        this.maxRychlost = maxRychlost;
        this.spotrebaPaliva = spotrebaPaliva;
        this.typPohonu = typPohonu;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public int getMaxRychlost() {
        return maxRychlost;
    }

    public void setMaxRychlost(int maxRychlost) {
        this.maxRychlost = maxRychlost;
    }

    public double getSpotrebaPaliva() {
        return spotrebaPaliva;
    }

    public void setSpotrebaPaliva(double spotrebaPaliva) {
        this.spotrebaPaliva = spotrebaPaliva;
    }

    public String getTypPohonu() {
        return typPohonu;
    }

    public void setTypPohonu(String typPohonu) {
        this.typPohonu = typPohonu;
    }
}

