package Classes;

public class NakladniVoz extends Vozidlo{

    private int vaha;

    public NakladniVoz(String nazev, int maxRychlost, double spotrebaPaliva, String typPohonu, int vaha) {
        super(nazev, maxRychlost, spotrebaPaliva, typPohonu);
        this.vaha = vaha;
    }

    public int getVaha() {
        return vaha;
    }

    public void setVaha(int vaha) {
        this.vaha = vaha;
    }
}
